module Data.ServerState where

import Control.Concurrent (MVar, modifyMVar_)
import Data.Map.Strict (Map, (!))
import qualified Data.Map.Strict as Map
import Data.Set (Set)
import Data.AnswerLabel (AnswerLabel)
import Network.WebSockets (Connection)
import Data.Quiz (Quiz, Round, answerByLabel, correct)

type ServerState = Map String QuizState

init :: ServerState
init = Map.empty

data QuizState = QuizState
    { currentQuestion :: (Int, Round)
    , remainingQuestions :: [(Int, Round)]
    , state :: State
    , quizmaster :: Connection
    , participants :: Map String Participant
    , presenters :: [(Int, Connection)]
    }

createQuiz :: (String, Connection, Quiz) -> ServerState -> ServerState
createQuiz (key, quizmaster, quiz) serverState =
    Map.insert key quizState serverState
    where
        quizState = QuizState
            { currentQuestion = x
            , remainingQuestions = xs
            , state = Initial
            , quizmaster = quizmaster
            , participants = Map.empty
            , presenters = []
            }
        (x:xs) = zip [0..] quiz

modifyQuiz :: MVar ServerState -> String -> (QuizState -> IO QuizState) -> IO ()
modifyQuiz serverState key f =
    modifyMVar_ serverState $ \s -> do
        newQuiz <- f $ s Map.! key
        return $ Map.insert key newQuiz s

registerParticipant :: (String, Connection) -> QuizState -> QuizState
registerParticipant (nickname, connection) quizState =
    quizState
        { participants =
            Map.insert nickname newParticipant $ participants quizState
        }
    where
        newParticipant = Participant
            { connection = connection
            , score = 0
            }

data Participant = Participant
    { connection :: Connection
    , score :: Int
    }

data State
    = Initial
    | QuestionAsked
        { remainingParticipants :: Set String
        , answers :: [(String, AnswerLabel)]
        }
    | Paused
        { answers :: [(String, AnswerLabel)] }
