{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
module Data.QuizmasterMessage where

import Data.Aeson
import Data.Aeson.Types
import Data.Quiz (Quiz)
import Data.Text (Text)
import GHC.Generics (Generic)

data QuizmasterMessage
    = Go
    | Next
    deriving (Generic)

instance FromJSON QuizmasterMessage where
    parseJSON =
        genericParseJSON defaultOptions
            { allNullaryToStringTag = False }
