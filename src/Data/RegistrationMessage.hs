{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
module Data.RegistrationMessage where

import Data.Aeson
import Data.Quiz (Quiz)
import Data.Text (Text)
import GHC.Generics (Generic)

data RegistrationMessage
    = RegisterQuizmaster { quiz :: Quiz }
    | RegisterParticipant { key :: String, nickname :: String }
    | RegisterPresenter { key :: String }
    deriving (Generic, FromJSON)
