{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}
module Data.ServerMessage where

import qualified Data.Quiz as Q
import Data.Quiz (Round)
import Data.Answers (Answers(..), a, b, c, d)
import Data.Aeson
import Data.Aeson.Types
import GHC.Generics (Generic)

data ServerMessage
    = QuizCreated { key :: String }
    | ReportLobby { nicknames :: [String] }
    | ReportStatus
    | DisplayLobby
    | LobbyWait
    | AskQuestion { id_ :: Int, question :: String, answers :: Answers String }
    | ConfirmAnswerGiven
    | GiveFeedback { correct, moreQuestions :: Bool }
    | ReportResults
        { guesses :: Answers Guess
        , participantScores :: [ParticipantScore]
        , moreQuestions :: Bool
        }
    deriving (Generic)

data Guess = Guess
    { correct_ :: Bool
    , count :: Int
    } deriving (Generic)

instance ToJSON Guess where
    toJSON =
        genericToJSON defaultOptions
            { fieldLabelModifier = \case
                "correct_" -> "correct"
                xs         -> xs
            }

data ParticipantScore = ParticipantScore
    { nickname :: String
    , score :: Int
    } deriving (Generic, ToJSON)

instance ToJSON ServerMessage where
    toJSON =
        genericToJSON defaultOptions
            { fieldLabelModifier =
                \case "id_" -> "id"
                      xs    -> xs
            }

buildAskQuestion :: (Int, Round) -> ServerMessage
buildAskQuestion (id_, round) = AskQuestion
    { id_ = id_
    , question = Q.question round
    , answers = Answers
        { a = Q.text . a . Q.answers $ round
        , b = Q.text . b . Q.answers $ round
        , c = Q.text . c . Q.answers $ round
        , d = Q.text . d . Q.answers $ round
        }
    }
