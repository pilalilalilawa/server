{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
module Data.Quiz where

import Data.Aeson
import Data.AnswerLabel (AnswerLabel (..))
import Data.Answers (Answers, a, b, c, d)
import GHC.Generics (Generic)

type Quiz = [Round]

data Round = Round
    { question :: String
    , answers :: Answers Answer
    } deriving (Generic, FromJSON)

data Answer = Answer
    { text :: String
    , correct :: Bool
    } deriving (Generic, FromJSON)

answerByLabel :: AnswerLabel -> Round -> Answer
answerByLabel label = f label . answers
    where
        f A = a
        f B = b
        f C = c
        f D = d
