{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
module Data.AnswerLabel where

import Data.Aeson
import Data.Text (unpack)
import GHC.Generics

data AnswerLabel = A | B | C | D deriving (Eq, Generic, FromJSON)
