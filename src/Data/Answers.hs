{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
module Data.Answers where

import Data.Aeson
import Data.Aeson.Types
import GHC.Generics (Generic)

data Answers a = Answers
    { a, b, c, d :: a
    } deriving (Generic)

instance (ToJSON a) => ToJSON (Answers a)
instance (FromJSON a) => FromJSON (Answers a)
