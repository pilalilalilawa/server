{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
module Data.ParticipantMessage where

import Data.Aeson
import Data.Aeson.Types
import Data.AnswerLabel (AnswerLabel)
import Data.Text (Text)
import GHC.Generics (Generic)

data ParticipantMessage
    = GiveAnswer { questionId :: Int, answer :: AnswerLabel }
    deriving (Generic)

instance FromJSON ParticipantMessage where
    parseJSON =
        genericParseJSON defaultOptions
            { tagSingleConstructors = True }
