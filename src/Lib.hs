module Lib
    ( runBackend
    ) where

import Network.WebSockets
import Control.Exception (finally)
import Data.AnswerLabel (AnswerLabel(..))
import Data.Answers (Answers(..), a, b, c, d)
import Data.ServerMessage
    ( ServerMessage
        ( ConfirmAnswerGiven
        , DisplayLobby
        , GiveFeedback
        , LobbyWait
        , QuizCreated
        , ReportLobby
        , ReportResults
        , ReportStatus
        )
    , nicknames
    , buildAskQuestion
    , ParticipantScore (..)
    , participantScores
    , Guess (..)
    , participantScores
    , guesses
    , moreQuestions
    )
import qualified Data.ServerMessage as ServerMessage
import Data.RegistrationMessage (RegistrationMessage(..))
import Data.QuizmasterMessage (QuizmasterMessage(..))
import Data.ParticipantMessage (ParticipantMessage(..))
import Data.Aeson (encode, decode)
import Data.Aeson.Types (FromJSON)
import Data.Maybe (fromJust, catMaybes)
import Data.Quiz hiding (answers)
import qualified Data.Set as Set
import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map, (!))
import Control.Concurrent (MVar, newMVar, modifyMVar_, forkIO, threadDelay)
import Control.Monad (forM_, forever)
import System.Random (randomRIO)
import Data.ServerState
    ( createQuiz
    , registerParticipant
    , modifyQuiz
    , ServerState
    , QuizState
    , Participant
    , State
    , currentQuestion
    , remainingQuestions
    , state
    , quizmaster
    , participants
    , presenters
    , State
        ( Initial
        , QuestionAsked
        , Paused
        )
    , remainingParticipants
    , answers
    , connection
    , score
    )
import qualified Data.ServerState as ServerState

runBackend :: IO ()
runBackend = do
    serverState <- newMVar ServerState.init
    runServer "127.0.0.1" 8080 $ handleConnection serverState

handleConnection :: MVar ServerState -> ServerApp
handleConnection serverState pending = do
    connection <- acceptRequest pending
    message <- receiveMessage connection
    case message of
        RegisterQuizmaster quiz -> do
            key <- generateKey
            modifyMVar_ serverState $ \s -> do
                sendMessage connection $ QuizCreated key
                return $ createQuiz (key, connection, quiz) s
            handleMessages
                serverState
                connection
                ()
                key
                handleQuizmaster
                disconnectQuizmaster
        RegisterPresenter key -> do
            id <- generateId
            modifyQuiz serverState key $ \q -> do
                putStrLn "Presenter joined"
                let newQuizState = q { presenters = (id, connection):presenters q }
                sendMessage connection $ presenterMessage newQuizState
                return newQuizState
            handleMessages
                serverState
                connection
                id
                key
                handlePresenter
                disconnectPresenter
            forever $ receive connection -- keep the presenter connection open
        RegisterParticipant key nickname -> do
            modifyQuiz serverState key $ \q -> do
                let newQuizState = registerParticipant (nickname, connection) q
                sendMessages newQuizState [SpecificParticipant nickname, Quizmaster]
                return newQuizState
            handleMessages
                serverState
                connection
                nickname
                key
                handleParticipant
                disconnectParticipant

generateId :: IO Int
generateId = randomRIO (minBound, maxBound)

disconnectQuizmaster :: () -> String -> Connection -> MVar ServerState -> IO ()
disconnectQuizmaster _ quizKey _ serverState =
    modifyMVar_ serverState $ \s ->
        return $ Map.delete quizKey s

disconnectPresenter :: Int -> String -> Connection -> MVar ServerState -> IO ()
disconnectPresenter presenterId quizKey connection serverState =
    modifyQuiz serverState quizKey $ \quizState ->
        return $ quizState
            { presenters =
                filter (\(id, _) -> id /= presenterId) . presenters $ quizState }

disconnectParticipant :: String -> String -> Connection -> MVar ServerState -> IO ()
disconnectParticipant nickname quizKey _ serverState =
    modifyQuiz serverState quizKey $ \quizState ->
        return $ quizState
            { participants = Map.delete nickname . participants $ quizState }

generateKey :: IO String
generateKey = leftPad (l, '0') . show <$> randomRIO (1 :: Int, 10^l-1) where l = 6

leftPad :: (Int, a) -> [a] -> [a]
leftPad (l, x) xs = replicate (l - length xs) x ++ xs

handleMessages :: (FromJSON message)
               => MVar ServerState
               -> Connection
               -> extra
               -> String
               -> (extra -> message -> QuizState -> (QuizState, [Broadcast], MVar ServerState -> String -> IO ()))
               -> (extra -> String -> Connection -> MVar ServerState -> IO ())
               -> IO ()
handleMessages serverState connection extra key handler disconnect =
    flip finally (disconnect extra key connection serverState) $ forever $ do
        message <- receiveMessage connection
        modifyQuiz serverState key $ \quizState ->
            let
                (newQuizState, broadcasts, io) =
                    handler extra message quizState
            in do
                sendMessages newQuizState broadcasts
                io serverState key
                return newQuizState

noIO :: a -> b -> IO ()
noIO = const . const $ return ()

sendMessages :: QuizState -> [Broadcast] -> IO ()
sendMessages quizState broadcasts = 
    forM_
        (concatMap (broadcastMessages quizState) broadcasts)
        (uncurry sendMessage)

broadcastMessages :: QuizState -> Broadcast -> [(Connection, ServerMessage)]
broadcastMessages quizState Quizmaster =
    [(quizmaster quizState, quizmasterMessage quizState)]
broadcastMessages quizState Presenters =
    map (\(_, connection) -> (connection, message)) . presenters $ quizState
    where message = presenterMessage quizState
broadcastMessages quizState Participants =
    map f . Map.toList . participants $ quizState
    where
        f (nickname, participant) =
            ( connection participant
            , participantMessage quizState nickname
            )
broadcastMessages quizState (SpecificParticipant nickname) =
    [ ( connection $ (participants quizState) ! nickname
      , participantMessage quizState nickname
      )
    ]

quizmasterMessage :: QuizState -> ServerMessage
quizmasterMessage quizState =
    case state quizState of
        Initial ->
            ReportLobby
                { nicknames = Map.keys . participants $ quizState }
        QuestionAsked _ _ ->
            ReportStatus
        Paused _ ->
            ReportStatus

presenterMessage :: QuizState -> ServerMessage
presenterMessage quizState =
    case state quizState of
        Initial ->
            DisplayLobby
        QuestionAsked _ _ ->
            buildAskQuestion . currentQuestion $ quizState
        Paused {answers = answers} ->
            ReportResults
                { guesses =
                    let
                        answerGuess label =
                            Guess
                                (correct . answerByLabel label . snd . currentQuestion $ quizState)
                                (length . filter (== label) . map snd $ answers)
                    in
                    Answers
                        { a = answerGuess A
                        , b = answerGuess B
                        , c = answerGuess C
                        , d = answerGuess D
                        }
                , participantScores =
                    map
                        (\(nickname, participant) ->
                            ParticipantScore
                                { ServerMessage.nickname = nickname
                                , ServerMessage.score = ServerState.score participant
                                }
                        )
                        (Map.toList $ participants quizState)
                , moreQuestions = not . null . remainingQuestions $ quizState
                }

participantMessage :: QuizState -> String -> ServerMessage
participantMessage quizState nickname =
    case state quizState of
        Initial ->
            LobbyWait
        QuestionAsked remainingParticipants _ ->
            if nickname `Set.member` remainingParticipants then
                buildAskQuestion . currentQuestion $ quizState
            else
                ConfirmAnswerGiven
        Paused answers ->
            GiveFeedback
                (maybe
                    False
                    (\answerLabel ->
                        correct . answerByLabel answerLabel . snd . currentQuestion $ quizState
                    )
                    (lookup nickname answers)
                )
                (not . null . remainingQuestions $ quizState)

data Broadcast
    = Quizmaster
    | Presenters
    | Participants
    | SpecificParticipant String

handleQuizmaster :: ()
                 -> QuizmasterMessage
                 -> QuizState
                 -> (QuizState, [Broadcast], MVar ServerState -> String -> IO ())
handleQuizmaster () message quizState =
    case (state quizState, message) of
        (Initial, Go) ->
            askQuestion quizState
        (Paused _, Next) ->
            case remainingQuestions quizState of
                x:xs ->
                    askQuestion $ quizState
                        { currentQuestion = x
                        , remainingQuestions = xs
                        }
                [] ->
                    (quizState, [], noIO)
        _ ->
            (quizState, [], noIO)
    where
        askQuestion q =
            ( q
                { state =
                    QuestionAsked
                        { remainingParticipants = Map.keysSet . participants $ q
                        , answers = []
                        }
                }
            , [Quizmaster, Presenters, Participants]
            , enforceQuestionTimeout . fst . currentQuestion $ q
            )

handlePresenter :: Int
                -> ()
                -> QuizState
                -> (QuizState, [Broadcast], MVar ServerState -> String -> IO ())
handlePresenter _ _ quizState = (quizState, [], noIO)

thinkingTime :: Int
thinkingTime = 20

enforceQuestionTimeout :: Int -> MVar ServerState -> String -> IO ()
enforceQuestionTimeout questionId serverState quizKey = do
    _ <- forkIO $ do
        threadDelay $ thinkingTime * 10^6
        modifyQuiz serverState quizKey $ \quizState ->
            if (fst . currentQuestion $ quizState) == questionId then do
                let newQuizState = closeRound quizState
                sendMessages newQuizState [Quizmaster, Presenters, Participants]
                return newQuizState
            else
                return quizState
    return ()

handleParticipant :: String
                  -> ParticipantMessage
                  -> QuizState
                  -> (QuizState, [Broadcast], MVar ServerState -> String -> IO ())
handleParticipant nickname message quizState =
    case (state quizState, message) of
        (QuestionAsked remainingParticipants answers, GiveAnswer questionId answerLabel)
            | all (\(n, _) -> n /= nickname) answers && questionId == (fst . currentQuestion $ quizState) ->
                let
                    newRemainingParticipants =
                        Set.delete nickname remainingParticipants
                    newQuizState =
                        quizState
                            { state =
                                (QuestionAsked
                                    newRemainingParticipants
                                    ((nickname, answerLabel):answers)
                                )
                            }
                in
                if Set.null newRemainingParticipants then
                    ( closeRound newQuizState
                    , [Quizmaster, Presenters, Participants]
                    , noIO
                    )
                else
                    ( newQuizState
                    , [SpecificParticipant nickname]
                    , noIO
                    )
        _ ->
            (quizState, [], noIO)

closeRound :: QuizState -> QuizState
closeRound quizState =
    quizState
        { state = Paused (answers . state $ quizState)
        , participants =
            foldl
                (\participants (nickname, answerLabel) ->
                    Map.adjust
                        (\participant -> participant
                            { ServerState.score = ServerState.score participant +
                                if correct . answerByLabel answerLabel . snd . currentQuestion $ quizState then
                                    1
                                else
                                    0
                            }
                        )
                        nickname
                        participants
                )
                (participants quizState)
                (answers . state $ quizState)
        }

sendMessage :: Connection -> ServerMessage -> IO ()
sendMessage connection = sendTextData connection . encode

receiveMessage :: (FromJSON a) => Connection -> IO a
receiveMessage connection = do
    dataMessage <- receiveDataMessage connection
    case dataMessage of
        Text messageString _ ->
            case decode messageString of
                Just message ->
                    return message
                Nothing ->
                    receiveMessage connection
        Binary _ ->
            receiveMessage connection

